<div align="center">
  <a href="https://gitlab.com/outertale/outertales">
    <img width="200" height="200" src="https://gitlab.com/outertale/outertales/-/raw/main/source/images/logo.png">
  </a>
  <br>
  <br>

[![version][version]][version-url]
[![licenses][licenses]][licenses-url]

  <br>
  
  <h1>Outer Tale</h1>
  <p>
    🏆 Notre essai au concours des trophées NSI 2024 🏆
  </p>
</div>

[licenses-url]: https://gitlab.com/outertale/outertales/-/blob/main/license.txt
[licenses]: https://img.shields.io/badge/licence-GPL%20v3.0-green

[version-url]: https://gitlab.com/outertale/outertales/-/commits/main
[version]: https://img.shields.io/badge/version-beta%200.1.0-blue

Amnésique et piégé au fin fond de l'espace, il ne reste que vous pour résoudre le lourd mystère qui pèse sur ce vaisseau spatial.

**Outer Tale** est un jeu d'aventure et de découverte, dans lequel vous jouez le rôle d'un astronaute qui enquête sur la mystérieuse disparition de tout son équipage. Arriverez-vous à comprendre ce qu'il s'est passé, et à vous enfuir ?

# Tutoriel
## Installation
Ce tutoriel a été testé sur Windows Powershell, avec la version `3.10.11` de Python. Il est recommandé de suivre les étapes d'installations suivantes dans un environnement virtuel.

1. Cloner le dépôt

`> git clone https://gitlab.com/outertale/outertales.git/`

`> cd outertales`

2. Installer les dépendances

`> pip install -r requirements.txt`

3. Lancer le programme

`> cd source`

`> python main.py`

### Erreurs communes
Si vous lancez l'application depuis Visual Studio Code ou un autre IDE, et que le lancement cause une erreur du type `No file '[...]' found in working directory '[...]'`, activez l'option "Execute in File Dir" dans les préférences de l'application. Une autre solution peut être de n'ouvrir que le fichier `main.py` dans l'IDE.

Si la même erreur apparaît dans un terminal, vérifiez que vous exécutez bien votre commande depuis le dossier `outertales/source`. Si vous l'exécutez depuis le dossier racine `outertales`, ça ne marchera pas.

## Jouer
Une fois le main.py démmarré, vous pouvez modifier le volume de la musique dans les paramètres, puis commencer la partie via le bouton "Jouer". 

# Licences
Le code présent sous ce dépôt a été entièrement développé par les membres de l'équipe et est publié sous licence [GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) conformément au règlement des trophées NSI.
Notez toutefois que les images, assets, musiques et textes n'entrent pas dans cette catégorie et ont des conditions un peu plus spécifiques :

* Les **polices d'écritures** customisées proviennent de [Google Fonts](https://fonts.google.com/) et sont toutes disponibles sous la [Open Font License](https://openfontlicense.org/).
* Les **images et textures** sont un mélange de contenu générés par Copilot, GPT-4 et Stable Diffusion remixés ainsi que de contenu original fait nous-même. Dans tous les cas, nous en avons la propriété (conformément aux conditions d'utilisations des applications utilisées) et les publions sous licence [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
* Les **textes** sont publiés sous licence [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) si appliquable.
* La video de présentation du projet (trouvable sur la page du projet sur le site des trophées NSI) est publiée sous licence [Creative Commons BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/).
